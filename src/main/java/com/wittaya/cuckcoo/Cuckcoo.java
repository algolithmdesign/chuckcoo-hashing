/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.cuckcoo;

/**
 *
 * @author AdMiN
 */
public class Cuckcoo {
    static int table1 = 11;
    static int table2 = 2;
    static int[][] hashTable = new int[table2][table1];
    static int[] pos = new int[table2];

    static void initTable() {
        for (int j = 0; j < table1; j++) {
            for (int i = 0; i < table2; i++) {
                hashTable[i][j] = Integer.MIN_VALUE;
            }
        }
    }

    static int hash(int function, int key) {
        if (function == 1) {
            return key % table1;
        } else if (function == 2) {
            return (key / table1) % table1;
        }
        return Integer.MIN_VALUE;
    }

    static void place(int key, int tableID, int count, int countContinue) {
        if (count == countContinue) {
            System.out.printf("%d unpositioned\n", key);
            System.out.println("Cycle present. Rehash.\n");
            return;
        }
        for (int i = 0; i < table2; i++) {
            pos[i] = hash(i + 1, key);
            if (hashTable[i][pos[i]] == key) {
                return;
            }
        }

if (hashTable[tableID][pos[tableID]] != Integer.MIN_VALUE) {
            int dis = hashTable[tableID][pos[tableID]];
            hashTable[tableID][pos[tableID]] = key;
            place(dis, (tableID + 1) % table2, count + 1, countContinue);
        } else {
            hashTable[tableID][pos[tableID]] = key;
        }
    }

    static void printTable() {
        System.out.println("Final hash table:");

        for (int i = 0; i < table2; i++) {
            for (int j = 0; j < table1; j++) {
                if (hashTable[i][j] == Integer.MIN_VALUE) {
                    System.out.print("- ");
                } else {
                    System.out.printf("%d ", hashTable[i][j]);
                }
            }
            System.out.println("");
        }
        System.out.println("");
    }

    static void cuckoo(int key[], int size) {
        initTable();

        for (int i = 0, count = 0; i < size; i++, count = 0) {
            place(key[i], 0, count, size);
        }

        printTable();
    }
    public static void main(String[] args) {
        int key1[] = { 20, 50, 53, 75, 100, 67, 105, 3, 36, 39 };
        int num1 = key1.length;

        cuckoo(key1, num1);

        int key2[] = { 20, 50, 53, 75, 100, 67, 105, 3, 36, 39, 30 };
        int num2 = key2.length;

        cuckoo(key2, num2);
    }

}
